-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -548.4951903530
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999958314 
   Number of Beta  Electrons                 15.9999958314 
   Total number of  Electrons                31.9999916628 
   Exchange energy                          -41.6603387542 
   Correlation energy                        -1.1770667620 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.8374055162 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.4951903530 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0004063538
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -548.4998243764
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 15.9999980200 
   Number of Beta  Electrons                 15.9999980200 
   Total number of  Electrons                31.9999960400 
   Exchange energy                          -41.5226836319 
   Correlation energy                        -1.1705724162 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.6932560481 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.4998243764 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0004037504
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -548.5025676655
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 15.9999955712 
   Number of Beta  Electrons                 15.9999955712 
   Total number of  Electrons                31.9999911423 
   Exchange energy                          -41.5663745377 
   Correlation energy                        -1.1727393602 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7391138979 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5025676655 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0004048808
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -548.5026183156
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 15.9999956769 
   Number of Beta  Electrons                 15.9999956769 
   Total number of  Electrons                31.9999913537 
   Exchange energy                          -41.5782195681 
   Correlation energy                        -1.1733367106 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7515562788 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5026183156 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0004049770
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -548.5026518218
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 15.9999953420 
   Number of Beta  Electrons                 15.9999953420 
   Total number of  Electrons                31.9999906840 
   Exchange energy                          -41.5765379139 
   Correlation energy                        -1.1732424103 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7497803242 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5026518218 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0004050206
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -548.5026517770
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 15.9999954195 
   Number of Beta  Electrons                 15.9999954195 
   Total number of  Electrons                31.9999908389 
   Exchange energy                          -41.5760408004 
   Correlation energy                        -1.1732205230 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7492613234 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5026517770 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0004049990
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -548.5026515964
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 15.9999954093 
   Number of Beta  Electrons                 15.9999954093 
   Total number of  Electrons                31.9999908186 
   Exchange energy                          -41.5761773805 
   Correlation energy                        -1.1732270560 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7494044365 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5026515964 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0004050028
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 7
   prop. index: 1
       Filename                          : inp.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.7833563571
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000004
      2       0.724035
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2      -0.022423
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000004
      2       0.701612
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 S      0.000000000000    0.000000000000    0.363168000000
               1 O      0.000000000000    1.228797000000   -0.363168000000
               2 O      0.000000000000   -1.228797000000   -0.363168000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 S      0.000000000000   -0.000000021143    0.397629715246
               1 O      0.000000000000    1.308388347582   -0.380398863872
               2 O      0.000000000000   -1.308388326440   -0.380398851374
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     3 
    Coordinates:
               0 S      0.000000000000   -0.000000049589    0.396325650360
               1 O      0.000000000000    1.272498799248   -0.379746839731
               2 O      0.000000000000   -1.272498749659   -0.379746810629
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     4 
    Coordinates:
               0 S      0.000000000000    0.000000035110    0.385405502358
               1 O      0.000000000000    1.272022393368   -0.374286740215
               2 O      0.000000000000   -1.272022428477   -0.374286762142
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     5 
    Coordinates:
               0 S      0.000000000000   -0.000000542580    0.390304718412
               1 O      0.000000000000    1.269305482751   -0.376736520875
               2 O      0.000000000000   -1.269304940171   -0.376736197537
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     6 
    Coordinates:
               0 S      0.000000000000    0.000000192411    0.389732272643
               1 O      0.000000000000    1.270182720146   -0.376450076208
               2 O      0.000000000000   -1.270182912557   -0.376450196435
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     7 
    Coordinates:
               0 S      0.000000000000   -0.000000055559    0.389752090694
               1 O      0.000000000000    1.270050197039   -0.376460060031
               2 O      0.000000000000   -1.270050141480   -0.376460030664
