#!/bin/bash
crdr=$(pwd)

cd ${crdr}/00_reagent/geom
$HOME/orca/orca inp > out
echo "00 done"
cd ${crdr}/01_pre-intermediate/geom
$HOME/orca/orca inp > out
echo "01 done"
cd ${crdr}/02_intermediate/geom
$HOME/orca/orca inp > out
echo "02 done"
cd ${crdr}/03_post-intermediate/geom
$HOME/orca/orca inp > out
echo "03 done"
cd ${crdr}/04_product/geom
$HOME/orca/orca inp > out
echo "04 done"
cd ${crdr}/just_so2/geom
$HOME/orca/orca inp > out
echo "so2 done"
