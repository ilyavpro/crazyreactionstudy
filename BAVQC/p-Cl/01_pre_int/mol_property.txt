-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1407.7303136209
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 55.0000033444 
   Number of Beta  Electrons                 55.0000033444 
   Total number of  Electrons               110.0000066887 
   Exchange energy                         -121.8650133938 
   Correlation energy                        -3.8518134822 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7168268759 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7303136209 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0070896259
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1407.7303132434
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 55.0000033866 
   Number of Beta  Electrons                 55.0000033866 
   Total number of  Electrons               110.0000067731 
   Exchange energy                         -121.8650072283 
   Correlation energy                        -3.8518113129 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7168185411 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7303132434 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0070894055
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.7873952898
        Electronic Contribution:
                  0    
      0       2.062669
      1       0.073123
      2       0.920833
        Nuclear Contribution:
                  0    
      0      -3.098448
      1      -0.335893
      2      -0.674466
        Total Dipole moment:
                  0    
      0      -1.035779
      1      -0.262770
      2       0.246368
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1407.7303132492
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 55.0000033864 
   Number of Beta  Electrons                 55.0000033864 
   Total number of  Electrons               110.0000067728 
   Exchange energy                         -121.8650113215 
   Correlation energy                        -3.8518112708 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7168225923 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7303132492 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0070894055
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 3
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        216.9600418485
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1407.7244402220
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0104679489
        Number of frequencies          :     51      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      20.488031
      7      32.875100
      8      50.158767
      9      53.718757
     10      79.320043
     11      99.118164
     12     146.533751
     13     163.083899
     14     195.651007
     15     282.964466
     16     324.474499
     17     341.596534
     18     407.176970
     19     427.647845
     20     441.035726
     21     490.273504
     22     491.097041
     23     503.493861
     24     557.155227
     25     650.771997
     26     694.051342
     27     773.773389
     28     827.917558
     29     838.650241
     30     954.934678
     31     975.324287
     32     1036.779364
     33     1097.238399
     34     1106.814223
     35     1121.730216
     36     1134.618519
     37     1207.761148
     38     1273.294598
     39     1320.386367
     40     1324.716428
     41     1424.184980
     42     1431.792844
     43     1520.507772
     44     1583.734679
     45     1621.634382
     46     2430.621286
     47     3190.835208
     48     3196.029757
     49     3205.798946
     50     3208.059779
        Zero Point Energy (Hartree)    :          0.1008313019
        Inner Energy (Hartree)         :      -1407.6103084283
        Enthalpy (Hartree)             :      -1407.6093642193
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0155726495
        Vibrational entropy            :          0.0192931051
        Translational entropy          :          0.0155726495
        Entropy                        :          0.0548339598
        Gibbs Energy (Hartree)         :      -1407.6641981791
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.072471253935    0.061612215202   -0.194660085184
               1 C      0.686800456845    1.210784186249   -0.345443145196
               2 C      0.505403656528   -1.064521096296    0.384122599851
               3 C      2.023575356234    1.233145495416    0.082057552597
               4 C      2.590364735804    0.086869216622    0.664929613158
               5 C      1.829343277721   -1.060841990628    0.814583809633
               6 H      0.249290268579    2.095744593844   -0.795135223355
               7 C      2.811739802233    2.398274157523   -0.064916960473
               8 H     -1.105640692594    0.036642775636   -0.522948563933
               9 Cl    -0.451090357497   -2.513298549159    0.573691987454
              10 H      3.623170590688    0.113223923609    0.995565731196
              11 H      2.259017097730   -1.949819165410    1.263097820734
              12 N      3.521515837994    3.312580459208   -0.158962312657
              13 O      4.296146416036    4.243122874482   -0.231700225909
              14 O      5.478959495730    1.949526508032    1.466607114707
              15 S      6.414981067710    2.962788403685    0.933070207945
              16 O      7.172794244195    2.591865991984   -0.272229920568
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.072597923047    0.061647622915   -0.194515475809
               1 C      0.686766542246    1.210737333349   -0.345408865152
               2 C      0.505233660698   -1.064539185545    0.384244127169
               3 C      2.023591991126    1.233005164669    0.081945010846
               4 C      2.590313211365    0.086671662482    0.664787128150
               5 C      1.829204261405   -1.060972686692    0.814560052091
               6 H      0.249260959429    2.095713079542   -0.795082141292
               7 C      2.811914818815    2.398008678408   -0.065115003598
               8 H     -1.105802390308    0.036771736335   -0.522694898563
               9 Cl    -0.451497031410   -2.513112995937    0.573876499119
              10 H      3.623158547460    0.112939749843    0.995313706846
              11 H      2.258838244578   -1.949979094735    1.263056754631
              12 N      3.521844374302    3.312128372622   -0.159848600683
              13 O      4.295915639535    4.243230134620   -0.231399506468
              14 O      5.479287896328    1.949331421152    1.466365675452
              15 S      6.415081035677    2.963128797354    0.933439232305
              16 O      7.173386161802    2.592990209619   -0.271793695043
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.072597923047    0.061647622915   -0.194515475809
               1 C      0.686766542246    1.210737333349   -0.345408865152
               2 C      0.505233660698   -1.064539185545    0.384244127169
               3 C      2.023591991126    1.233005164669    0.081945010846
               4 C      2.590313211365    0.086671662482    0.664787128150
               5 C      1.829204261405   -1.060972686692    0.814560052091
               6 H      0.249260959429    2.095713079542   -0.795082141292
               7 C      2.811914818815    2.398008678408   -0.065115003598
               8 H     -1.105802390308    0.036771736335   -0.522694898563
               9 Cl    -0.451497031410   -2.513112995937    0.573876499119
              10 H      3.623158547460    0.112939749843    0.995313706846
              11 H      2.258838244578   -1.949979094735    1.263056754631
              12 N      3.521844374302    3.312128372622   -0.159848600683
              13 O      4.295915639535    4.243230134620   -0.231399506468
              14 O      5.479287896328    1.949331421152    1.466365675452
              15 S      6.415081035677    2.963128797354    0.933439232305
              16 O      7.173386161802    2.592990209619   -0.271793695043
