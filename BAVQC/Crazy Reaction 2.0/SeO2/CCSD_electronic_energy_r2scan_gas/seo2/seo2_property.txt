-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -2549.5611205377
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    32.9538    34.0000     1.0462     3.7548     3.7548     0.0000
  1   0     8.5231     8.0000    -0.5231     2.0113     2.0113    -0.0000
  2   0     8.5231     8.0000    -0.5231     2.0113     2.0113    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            34               1            8                1.877382
                0            34               2            8                1.877382
                1             8               2            8                0.133955
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     50
        Number of correlated Electrons                                                28
        Number of alpha correlated Electrons                                          14
        Number of beta correlated Electrons                                           14
        Reference Energy:                                               -2549.5611205372
        Total Correlation Energy:                                          -0.7480055099
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0378428913
        Total MDCI Energy:                                              -2550.3091260472
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -2549.5795246615
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    32.9025    34.0000     1.0975     3.7709     3.7709     0.0000
  1   0     8.5488     8.0000    -0.5488     2.0042     2.0042     0.0000
  2   0     8.5488     8.0000    -0.5488     2.0042     2.0042    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            34               1            8                1.885472
                0            34               2            8                1.885472
                1             8               2            8                0.118692
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     50
        Number of correlated Electrons                                                28
        Number of alpha correlated Electrons                                          14
        Number of beta correlated Electrons                                           14
        Reference Energy:                                               -2549.5795246619
        Total Correlation Energy:                                          -0.8504682124
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0432311494
        Total MDCI Energy:                                              -2550.4299928743
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -2549.5611205372 
       SCF energy with basis  cc-pVQZ :  -2549.5795246619 
       CBS SCF Energy                    :  -2549.5850698969 
       Correlation energy with basis  cc-pVTZ :  -2549.5611205372 
       Correlation energy with basis  cc-pVQZ :  -2549.5795246619 
       CBS Correlation Energy            :     -0.9234103328 
       CBS Total Energy                  :  -2550.5084802296 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       3
     number of electrons:                   50
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             103
     number of aux C basis functions:       490
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14394-p3fE/seo2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.3730063336
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000003
      2       3.946746
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000001
      2      -2.619730
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000002
      2       1.327016
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 Se     0.000000000000    0.000000000000    0.463500000000
               1 O      0.000000000000    1.362452000000   -0.413334000000
               2 O      0.000000000000   -1.362451000000   -0.413334000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 Se     0.000000000000    0.000000000000    0.463500000000
               1 O      0.000000000000    1.362452000000   -0.413334000000
               2 O      0.000000000000   -1.362451000000   -0.413334000000
