-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -2946.8906751867
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1405     6.0000    -0.1405     3.7782     3.7782    -0.0000
  1   0     6.1131     6.0000    -0.1131     3.8506     3.8506     0.0000
  2   0     6.1137     6.0000    -0.1137     3.8223     3.8223    -0.0000
  3   0     5.9749     6.0000     0.0251     3.7863     3.7863     0.0000
  4   0     6.1171     6.0000    -0.1171     3.8569     3.8569     0.0000
  5   0     6.1413     6.0000    -0.1413     3.7838     3.7838     0.0000
  6   0     0.8188     1.0000     0.1812     0.9772     0.9772    -0.0000
  7   0     5.6635     6.0000     0.3365     4.3125     4.3125    -0.0000
  8   0     0.8336     1.0000     0.1664     0.9749     0.9749    -0.0000
  9   0     0.8323     1.0000     0.1677     0.9753     0.9753     0.0000
 10   0     0.8260     1.0000     0.1740     0.9790     0.9790    -0.0000
 11   0     0.8350     1.0000     0.1650     0.9751     0.9751     0.0000
 12   0     6.9965     7.0000     0.0035     2.3248     2.3248    -0.0000
 13   0     8.6278     8.0000    -0.6278     1.8727     1.8727     0.0000
 14   0     8.4824     8.0000    -0.4824     2.0178     2.0178    -0.0000
 15   0    32.9002    34.0000     1.0998     3.7840     3.7840     0.0000
 16   0     8.5832     8.0000    -0.5832     1.9500     1.9500    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.390505
                0             6               2            6                1.358018
                0             6               8            1                0.972221
                1             6               3            6                1.309374
                1             6               6            1                0.958799
                2             6               5            6                1.361685
                2             6               9            1                0.973863
                3             6               4            6                1.313374
                3             6               7            6                0.842211
                3             6              12            7                0.295371
                4             6               5            6                1.391647
                4             6              10            1                0.963432
                5             6              11            1                0.972483
                7             6              12            7                1.738151
                7             6              14            8                1.645010
               12             7              13            8                0.120894
               13             8              15           34                1.621303
               14             8              15           34                0.283691
               15            34              16            8                1.844161
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.8906751911
        Total Correlation Energy:                                          -2.3604525790
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1208467686
        Total MDCI Energy:                                              -2949.2511277701
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -2946.9346211969
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1014     6.0000    -0.1014     3.7705     3.7705    -0.0000
  1   0     6.0847     6.0000    -0.0847     3.8620     3.8620    -0.0000
  2   0     6.1174     6.0000    -0.1174     3.8449     3.8449     0.0000
  3   0     6.0026     6.0000    -0.0026     3.6855     3.6855     0.0000
  4   0     6.0834     6.0000    -0.0834     3.8609     3.8609     0.0000
  5   0     6.1044     6.0000    -0.1044     3.7773     3.7773    -0.0000
  6   0     0.8362     1.0000     0.1638     0.9913     0.9913     0.0000
  7   0     5.6152     6.0000     0.3848     4.3695     4.3695    -0.0000
  8   0     0.8432     1.0000     0.1568     0.9853     0.9853    -0.0000
  9   0     0.8319     1.0000     0.1681     0.9828     0.9828     0.0000
 10   0     0.8418     1.0000     0.1582     0.9913     0.9913     0.0000
 11   0     0.8450     1.0000     0.1550     0.9855     0.9855     0.0000
 12   0     7.0572     7.0000    -0.0572     2.3831     2.3831    -0.0000
 13   0     8.6494     8.0000    -0.6494     1.8644     1.8644    -0.0000
 14   0     8.5124     8.0000    -0.5124     1.9877     1.9877     0.0000
 15   0    32.8566    34.0000     1.1434     3.8168     3.8168     0.0000
 16   0     8.6174     8.0000    -0.6174     1.9346     1.9346     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.396514
                0             6               2            6                1.372474
                0             6               8            1                0.984997
                1             6               3            6                1.279081
                1             6               6            1                0.982052
                2             6               5            6                1.376362
                2             6               9            1                0.983435
                3             6               4            6                1.277451
                3             6               7            6                0.881374
                3             6              12            7                0.286676
                4             6               5            6                1.399278
                4             6              10            1                0.986054
                5             6              11            1                0.985162
                7             6              12            7                1.804699
                7             6              14            8                1.618997
               12             7              13            8                0.108099
               13             8              15           34                1.644030
               14             8              15           34                0.286002
               15            34              16            8                1.846480
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.9346211910
        Total Correlation Energy:                                          -2.5537723320
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1329822212
        Total MDCI Energy:                                              -2949.4883935231
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -2946.8906751911 
       SCF energy with basis  cc-pVQZ :  -2946.9346211910 
       CBS SCF Energy                    :  -2946.9478622932 
       Correlation energy with basis  cc-pVTZ :  -2946.8906751911 
       Correlation energy with basis  cc-pVQZ :  -2946.9346211910 
       CBS Correlation Energy            :     -2.6913946311 
       CBS Total Energy                  :  -2949.6392569243 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   112
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             443
     number of aux C basis functions:       1953
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14391-UZP0/ts2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.3219907659
        Electronic Contribution:
                  0    
      0      33.403478
      1      20.973948
      2      -7.652643
        Nuclear Contribution:
                  0    
      0     -34.583559
      1     -23.109801
      2       8.134063
        Total Dipole moment:
                  0    
      0      -1.180081
      1      -2.135853
      2       0.481420
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.267023000000    0.045203000000   -0.488730000000
               1 C      1.368089000000    0.886626000000   -0.528989000000
               2 C      0.252221000000   -1.042727000000    0.385060000000
               3 C      2.461221000000    0.598376000000    0.293186000000
               4 C      2.445277000000   -0.466509000000    1.197435000000
               5 C      1.335583000000   -1.297944000000    1.225903000000
               6 H      1.401947000000    1.750297000000   -1.185344000000
               7 C      3.811613000000    1.334064000000    0.010831000000
               8 H     -0.588102000000    0.243721000000   -1.127178000000
               9 H     -0.619610000000   -1.689313000000    0.421932000000
              10 H      3.294173000000   -0.628408000000    1.853640000000
              11 H      1.306664000000   -2.137511000000    1.913329000000
              12 N      3.464916000000    2.147471000000    0.896258000000
              13 O      5.268611000000    3.284823000000    0.730165000000
              14 O      4.683431000000    1.037992000000   -0.834596000000
              15 Se     5.956603000000    2.950841000000   -0.743822000000
              16 O      5.264738000000    3.779999000000   -1.948376000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C      0.267023000000    0.045203000000   -0.488730000000
               1 C      1.368089000000    0.886626000000   -0.528989000000
               2 C      0.252221000000   -1.042727000000    0.385060000000
               3 C      2.461221000000    0.598376000000    0.293186000000
               4 C      2.445277000000   -0.466509000000    1.197435000000
               5 C      1.335583000000   -1.297944000000    1.225903000000
               6 H      1.401947000000    1.750297000000   -1.185344000000
               7 C      3.811613000000    1.334064000000    0.010831000000
               8 H     -0.588102000000    0.243721000000   -1.127178000000
               9 H     -0.619610000000   -1.689313000000    0.421932000000
              10 H      3.294173000000   -0.628408000000    1.853640000000
              11 H      1.306664000000   -2.137511000000    1.913329000000
              12 N      3.464916000000    2.147471000000    0.896258000000
              13 O      5.268611000000    3.284823000000    0.730165000000
              14 O      4.683431000000    1.037992000000   -0.834596000000
              15 Se     5.956603000000    2.950841000000   -0.743822000000
              16 O      5.264738000000    3.779999000000   -1.948376000000
