-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.5865812923
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1379     6.0000    -0.1379     3.7858     3.7858    -0.0000
  1   0     6.1325     6.0000    -0.1325     3.8579     3.8579    -0.0000
  2   0     6.1298     6.0000    -0.1298     3.8257     3.8257     0.0000
  3   0     5.9574     6.0000     0.0426     3.7651     3.7651     0.0000
  4   0     6.1293     6.0000    -0.1293     3.8513     3.8513     0.0000
  5   0     6.1373     6.0000    -0.1373     3.7803     3.7803     0.0000
  6   0     0.8295     1.0000     0.1705     0.9802     0.9802    -0.0000
  7   0     5.6905     6.0000     0.3095     4.2823     4.2823    -0.0000
  8   0     0.8350     1.0000     0.1650     0.9748     0.9748    -0.0000
  9   0     0.8334     1.0000     0.1666     0.9758     0.9758    -0.0000
 10   0     0.8244     1.0000     0.1756     0.9795     0.9795     0.0000
 11   0     0.8340     1.0000     0.1660     0.9746     0.9746     0.0000
 12   0     6.9917     7.0000     0.0083     2.2854     2.2854    -0.0000
 13   0     8.5239     8.0000    -0.5239     1.9479     1.9479     0.0000
 14   0     8.4570     8.0000    -0.4570     2.0512     2.0512    -0.0000
 15   0    15.0709    16.0000     0.9291     3.9084     3.9084     0.0000
 16   0     8.4854     8.0000    -0.4854     2.0265     2.0265     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.387186
                0             6               2            6                1.366867
                0             6               8            1                0.972613
                1             6               3            6                1.325987
                1             6               6            1                0.965852
                2             6               5            6                1.362855
                2             6               9            1                0.974446
                3             6               4            6                1.320562
                3             6               7            6                0.889279
                3             6              12            7                0.201518
                4             6               5            6                1.386385
                4             6              10            1                0.962404
                5             6              11            1                0.972484
                7             6              12            7                1.698746
                7             6              14            8                1.624122
               12             7              13            8                0.230800
               13             8              15           16                1.604742
               14             8              15           16                0.336657
               15            16              16            8                1.924635
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.5865812992
        Total Correlation Energy:                                          -2.3083604460
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1173691383
        Total MDCI Energy:                                               -946.8949417452
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.6352211191
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1004     6.0000    -0.1004     3.7776     3.7776     0.0000
  1   0     6.1034     6.0000    -0.1034     3.8731     3.8731     0.0000
  2   0     6.1337     6.0000    -0.1337     3.8521     3.8521     0.0000
  3   0     5.9948     6.0000     0.0052     3.6686     3.6686     0.0000
  4   0     6.1038     6.0000    -0.1038     3.8745     3.8745    -0.0000
  5   0     6.0979     6.0000    -0.0979     3.7712     3.7712    -0.0000
  6   0     0.8464     1.0000     0.1536     0.9938     0.9938     0.0000
  7   0     5.6162     6.0000     0.3838     4.3165     4.3165     0.0000
  8   0     0.8453     1.0000     0.1547     0.9855     0.9855     0.0000
  9   0     0.8334     1.0000     0.1666     0.9838     0.9838    -0.0000
 10   0     0.8425     1.0000     0.1575     0.9944     0.9944     0.0000
 11   0     0.8440     1.0000     0.1560     0.9855     0.9855    -0.0000
 12   0     7.0592     7.0000    -0.0592     2.3455     2.3455     0.0000
 13   0     8.5608     8.0000    -0.5608     1.9000     1.9000     0.0000
 14   0     8.4802     8.0000    -0.4802     2.0242     2.0242     0.0000
 15   0    14.9940    16.0000     1.0060     3.8943     3.8943    -0.0000
 16   0     8.5441     8.0000    -0.5441     1.9628     1.9628     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.397610
                0             6               2            6                1.382222
                0             6               8            1                0.985234
                1             6               3            6                1.290302
                1             6               6            1                0.989262
                2             6               5            6                1.378055
                2             6               9            1                0.983979
                3             6               4            6                1.290697
                3             6               7            6                0.930420
                3             6              12            7                0.183395
                4             6               5            6                1.394676
                4             6              10            1                0.986304
                5             6              11            1                0.985187
                7             6              12            7                1.765816
                7             6              14            8                1.586160
               12             7              13            8                0.224147
               13             8              15           16                1.593811
               14             8              15           16                0.354854
               15            16              16            8                1.892925
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6352211197
        Total Correlation Energy:                                          -2.4493586182
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1279901570
        Total MDCI Energy:                                               -947.0845797378
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.5865812992 
       SCF energy with basis  cc-pVQZ :   -944.6352211197 
       CBS SCF Energy                    :   -944.6498764885 
       Correlation energy with basis  cc-pVTZ :   -944.5865812992 
       Correlation energy with basis  cc-pVQZ :   -944.6352211197 
       CBS Correlation Energy            :     -2.5497337342 
       CBS Total Energy                  :   -947.1996102226 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14377-k4W8/ts2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.0067196489
        Electronic Contribution:
                  0    
      0       9.351114
      1       8.363387
      2      -1.184411
        Nuclear Contribution:
                  0    
      0     -11.134695
      1      -9.080820
      2       1.613440
        Total Dipole moment:
                  0    
      0      -1.783581
      1      -0.717433
      2       0.429029
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.408535714000   -0.439324259000    0.146052241000
               1 C     -1.421826226000    0.508652826000   -0.096736458000
               2 C     -2.055758039000   -1.765593982000    0.401308329000
               3 C     -0.084820406000    0.106421469000   -0.052392194000
               4 C      0.283988655000   -1.223577174000    0.167061178000
               5 C     -0.715536695000   -2.157573644000    0.408887612000
               6 H     -1.665151450000    1.539242747000   -0.313149118000
               7 C      1.024940101000    1.183673546000   -0.050521010000
               8 H     -3.449725394000   -0.150110036000    0.124773797000
               9 H     -2.828757402000   -2.499212873000    0.583737511000
              10 H      1.329259225000   -1.499323099000    0.152691079000
              11 H     -0.454160408000   -3.190505136000    0.590320441000
              12 N      1.089781256000    1.100197029000   -1.298536154000
              13 O      2.404638588000    2.463600877000   -1.496480371000
              14 O      1.564869758000    1.782179254000    0.913576135000
              15 S      3.186287335000    2.632041480000   -0.211695775000
              16 O      4.200506818000    1.609210978000    0.031102759000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.408535714000   -0.439324259000    0.146052241000
               1 C     -1.421826226000    0.508652826000   -0.096736458000
               2 C     -2.055758039000   -1.765593982000    0.401308329000
               3 C     -0.084820406000    0.106421469000   -0.052392194000
               4 C      0.283988655000   -1.223577174000    0.167061178000
               5 C     -0.715536695000   -2.157573644000    0.408887612000
               6 H     -1.665151450000    1.539242747000   -0.313149118000
               7 C      1.024940101000    1.183673546000   -0.050521010000
               8 H     -3.449725394000   -0.150110036000    0.124773797000
               9 H     -2.828757402000   -2.499212873000    0.583737511000
              10 H      1.329259225000   -1.499323099000    0.152691079000
              11 H     -0.454160408000   -3.190505136000    0.590320441000
              12 N      1.089781256000    1.100197029000   -1.298536154000
              13 O      2.404638588000    2.463600877000   -1.496480371000
              14 O      1.564869758000    1.782179254000    0.913576135000
              15 S      3.186287335000    2.632041480000   -0.211695775000
              16 O      4.200506818000    1.609210978000    0.031102759000
