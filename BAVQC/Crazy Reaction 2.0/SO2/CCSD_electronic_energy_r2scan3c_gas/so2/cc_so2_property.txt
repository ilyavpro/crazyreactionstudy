-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -547.2779729901
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.1225    16.0000     0.8775     3.8862     3.8862    -0.0000
  1   0     8.4387     8.0000    -0.4387     2.0620     2.0620    -0.0000
  2   0     8.4387     8.0000    -0.4387     2.0620     2.0620     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.943110
                0            16               2            8                1.943110
                1             8               2            8                0.118849
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     32
        Number of correlated Electrons                                                18
        Number of alpha correlated Electrons                                           9
        Number of beta correlated Electrons                                            9
        Reference Energy:                                                -547.2779729900
        Total Correlation Energy:                                          -0.6901354790
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0329112170
        Total MDCI Energy:                                               -547.9681084690
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -547.3018578227
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 2
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.0359    16.0000     0.9641     3.8329     3.8329    -0.0000
  1   0     8.4821     8.0000    -0.4821     2.0090     2.0090    -0.0000
  2   0     8.4821     8.0000    -0.4821     2.0090     2.0090     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.916439
                0            16               2            8                1.916439
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     32
        Number of correlated Electrons                                                18
        Number of alpha correlated Electrons                                           9
        Number of beta correlated Electrons                                            9
        Reference Energy:                                                -547.3018578227
        Total Correlation Energy:                                          -0.7403799360
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0368237623
        Total MDCI Energy:                                               -548.0422377586
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -547.2779729900 
       SCF energy with basis  cc-pVQZ :   -547.3018578227 
       CBS SCF Energy                    :   -547.3090544165 
       Correlation energy with basis  cc-pVTZ :   -547.2779729900 
       Correlation energy with basis  cc-pVQZ :   -547.3018578227 
       CBS Correlation Energy            :     -0.7761484365 
       CBS Total Energy                  :   -548.0852028531 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       3
     number of electrons:                   32
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             94
     number of aux C basis functions:       422
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14386-XdrL/cc_so2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.2670120446
        Electronic Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.914065
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2      -0.022171
        Total Dipole moment:
                  0    
      0       0.000000
      1       0.000000
      2       0.891893
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 S      0.000000000000    0.000000000000    0.384030000000
               1 O      0.000000000000    1.259580000000   -0.373599000000
               2 O      0.000000000000   -1.259580000000   -0.373599000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 S      0.000000000000    0.000000000000    0.384030000000
               1 O      0.000000000000    1.259580000000   -0.373599000000
               2 O      0.000000000000   -1.259580000000   -0.373599000000
