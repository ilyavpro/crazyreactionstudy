-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -397.4410398551
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 14
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1237     6.0000    -0.1237     3.7924     3.7924    -0.0000
  1   0     6.1871     6.0000    -0.1871     3.8325     3.8325    -0.0000
  2   0     6.1735     6.0000    -0.1735     3.8258     3.8258    -0.0000
  3   0     5.7806     6.0000     0.2194     3.8414     3.8414    -0.0000
  4   0     6.2233     6.0000    -0.2233     3.7982     3.7982    -0.0000
  5   0     6.1257     6.0000    -0.1257     3.8023     3.8023    -0.0000
  6   0     0.8396     1.0000     0.1604     0.9899     0.9899     0.0000
  7   0     5.6153     6.0000     0.3847     4.2765     4.2765    -0.0000
  8   0     0.8438     1.0000     0.1562     0.9758     0.9758     0.0000
  9   0     0.8455     1.0000     0.1545     0.9791     0.9791     0.0000
 10   0     0.8498     1.0000     0.1502     0.9843     0.9843     0.0000
 11   0     0.8432     1.0000     0.1568     0.9772     0.9772    -0.0000
 12   0     7.2287     7.0000    -0.2287     3.0855     3.0855    -0.0000
 13   0     8.3202     8.0000    -0.3202     2.2755     2.2755    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.376974
                0             6               2            6                1.378786
                0             6               8            1                0.975998
                1             6               3            6                1.371870
                1             6               6            1                0.969241
                2             6               5            6                1.374916
                2             6               9            1                0.977505
                3             6               4            6                1.329688
                3             6              12            7                1.028233
                4             6               5            6                1.377826
                4             6              10            1                0.979233
                5             6              11            1                0.974914
                7             6              12            7                2.012483
                7             6              13            8                2.174895
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     62
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -397.4410398565
        Total Correlation Energy:                                          -1.5954330799
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0779946274
        Total MDCI Energy:                                               -399.0364729364
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -397.4671412499
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 14
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1078     6.0000    -0.1078     3.8133     3.8133     0.0000
  1   0     6.1697     6.0000    -0.1697     3.8467     3.8467     0.0000
  2   0     6.1547     6.0000    -0.1547     3.8318     3.8318     0.0000
  3   0     5.7659     6.0000     0.2341     3.7990     3.7990    -0.0000
  4   0     6.1886     6.0000    -0.1886     3.8199     3.8199     0.0000
  5   0     6.1184     6.0000    -0.1184     3.8157     3.8157    -0.0000
  6   0     0.8352     1.0000     0.1648     0.9988     0.9988     0.0000
  7   0     5.3646     6.0000     0.6354     4.2821     4.2821     0.0000
  8   0     0.8443     1.0000     0.1557     0.9811     0.9811     0.0000
  9   0     0.8509     1.0000     0.1491     0.9902     0.9902     0.0000
 10   0     0.8731     1.0000     0.1269     1.0041     1.0041     0.0000
 11   0     0.8460     1.0000     0.1540     0.9850     0.9850    -0.0000
 12   0     7.4980     7.0000    -0.4980     3.1223     3.1223     0.0000
 13   0     8.3827     8.0000    -0.3827     2.2515     2.2515    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.396924
                0             6               2            6                1.392407
                0             6               8            1                0.981822
                1             6               3            6                1.380418
                1             6               6            1                0.978461
                2             6               5            6                1.387993
                2             6               9            1                0.990418
                3             6               4            6                1.333178
                3             6              12            7                1.016138
                4             6               5            6                1.388015
                4             6              10            1                1.008320
                5             6              11            1                0.982873
                7             6              12            7                2.080844
                7             6              13            8                2.130665
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     62
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -397.4671412479
        Total Correlation Energy:                                          -1.6846639336
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0843515057
        Total MDCI Energy:                                               -399.1518051815
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -397.4410398565 
       SCF energy with basis  cc-pVQZ :   -397.4671412479 
       CBS SCF Energy                    :   -397.4750056995 
       Correlation energy with basis  cc-pVTZ :   -397.4410398565 
       Correlation energy with basis  cc-pVQZ :   -397.4671412479 
       CBS Correlation Energy            :     -1.7481864400 
       CBS Total Energy                  :   -399.2231921395 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       14
     number of electrons:                   62
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             340
     number of aux C basis functions:       1463
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14384-H7Ci/cc_product.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.6640847479
        Electronic Contribution:
                  0    
      0       6.362616
      1       3.375662
      2      -1.661256
        Nuclear Contribution:
                  0    
      0      -7.258087
      1      -3.865026
      2       1.900400
        Total Dipole moment:
                  0    
      0      -0.895471
      1      -0.489364
      2       0.239144
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C      1.804373000000   -0.430949000000    0.365941000000
               1 C      2.883883000000    0.376435000000    0.028869000000
               2 C      1.993875000000   -1.778577000000    0.657117000000
               3 C      4.166461000000   -0.170760000000   -0.017344000000
               4 C      4.364439000000   -1.524100000000    0.274057000000
               5 C      3.276740000000   -2.319150000000    0.609373000000
               6 H      2.748757000000    1.428425000000   -0.199958000000
               7 C      6.424809000000    0.555112000000   -0.495671000000
               8 H      0.807664000000   -0.001294000000    0.400931000000
               9 H      1.147685000000   -2.405516000000    0.920014000000
              10 H      5.366571000000   -1.942506000000    0.235882000000
              11 H      3.434435000000   -3.369793000000    0.834893000000
              12 N      5.229817000000    0.670830000000   -0.360749000000
              13 O      7.584090000000    0.582442000000   -0.662355000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     2 
    Coordinates:
               0 C      1.804373000000   -0.430949000000    0.365941000000
               1 C      2.883883000000    0.376435000000    0.028869000000
               2 C      1.993875000000   -1.778577000000    0.657117000000
               3 C      4.166461000000   -0.170760000000   -0.017344000000
               4 C      4.364439000000   -1.524100000000    0.274057000000
               5 C      3.276740000000   -2.319150000000    0.609373000000
               6 H      2.748757000000    1.428425000000   -0.199958000000
               7 C      6.424809000000    0.555112000000   -0.495671000000
               8 H      0.807664000000   -0.001294000000    0.400931000000
               9 H      1.147685000000   -2.405516000000    0.920014000000
              10 H      5.366571000000   -1.942506000000    0.235882000000
              11 H      3.434435000000   -3.369793000000    0.834893000000
              12 N      5.229817000000    0.670830000000   -0.360749000000
              13 O      7.584090000000    0.582442000000   -0.662355000000
