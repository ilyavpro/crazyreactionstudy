-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -859.1467792817
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 38.9999825153 
   Number of Beta  Electrons                 38.9999825153 
   Total number of  Electrons                77.9999650307 
   Exchange energy                          -80.2963413972 
   Correlation energy                        -2.6745260259 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -82.9708674231 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.1467792817 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                854
     Surface Area:         657.0368736514
     Dielectric Energy:     -0.0049437557
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0052607193
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -859.1468332342
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 38.9999830904 
   Number of Beta  Electrons                 38.9999830904 
   Total number of  Electrons                77.9999661808 
   Exchange energy                          -80.2941101972 
   Correlation energy                        -2.6742935423 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -82.9684037395 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.1468332342 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                854
     Surface Area:         657.1916080329
     Dielectric Energy:     -0.0050220657
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0052607081
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -859.1468461274
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 38.9999835255 
   Number of Beta  Electrons                 38.9999835255 
   Total number of  Electrons                77.9999670511 
   Exchange energy                          -80.2928702758 
   Correlation energy                        -2.6741663719 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -82.9670366477 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.1468461274 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                854
     Surface Area:         657.2711072987
     Dielectric Energy:     -0.0050656359
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0052607891
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -859.1468465934
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 38.9999836128 
   Number of Beta  Electrons                 38.9999836128 
   Total number of  Electrons                77.9999672257 
   Exchange energy                          -80.2927430821 
   Correlation energy                        -2.6741533909 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -82.9668964730 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.1468465934 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                854
     Surface Area:         657.2765400312
     Dielectric Energy:     -0.0050702994
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0052608223
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 4
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.8763478799
        Electronic Contribution:
                  0    
      0      -1.764213
      1      -2.185801
      2      -0.133658
        Nuclear Contribution:
                  0    
      0       1.047547
      1       1.311272
      2       0.087286
        Total Dipole moment:
                  0    
      0      -0.716666
      1      -0.874530
      2      -0.046372
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -859.1468465943
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 38.9999836130 
   Number of Beta  Electrons                 38.9999836130 
   Total number of  Electrons                77.9999672260 
   Exchange energy                          -80.2927368791 
   Correlation energy                        -2.6741531149 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -82.9668899939 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.1468465943 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                854
     Surface Area:         657.2765400312
     Dielectric Energy:     -0.0050703630
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0052608223
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 5
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        152.9981414349
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -859.1462600167
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0057781244
        Number of frequencies          :     42      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      34.868184
      7      53.667003
      8     158.453053
      9     274.748154
     10     321.230649
     11     338.915041
     12     408.114369
     13     426.860838
     14     446.574006
     15     486.494723
     16     490.227758
     17     552.121478
     18     649.568967
     19     694.510900
     20     771.436746
     21     828.359105
     22     829.178935
     23     953.779329
     24     970.230539
     25     1033.173734
     26     1092.104007
     27     1119.784258
     28     1130.768986
     29     1202.649632
     30     1316.313393
     31     1322.243786
     32     1427.177363
     33     1430.538429
     34     1520.679713
     35     1582.670261
     36     1617.127016
     37     2422.994384
     38     3193.194149
     39     3193.662443
     40     3205.269677
     41     3207.501636
        Zero Point Energy (Hartree)    :          0.0927378086
        Inner Energy (Hartree)         :       -859.0449115409
        Enthalpy (Hartree)             :       -859.0439673318
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0143109852
        Vibrational entropy            :          0.0097790726
        Translational entropy          :          0.0143109852
        Entropy                        :          0.0435635772
        Gibbs Energy (Hartree)         :       -859.0875309091
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.079849496565   -0.050698250680    0.352726579034
               1 C      0.790792978189    1.025123533642    0.417567393811
               2 C      0.429678896684   -1.344549560522    0.407363047176
               3 C      2.175440457599    0.814632242175    0.536927872615
               4 C      2.666100298657   -0.501415291002    0.590594682520
               5 C      1.796387187333   -1.578174772004    0.525837097660
               6 H      0.405272278695    2.038244726470    0.376188210824
               7 C      3.064016692597    1.913005876628    0.602535028384
               8 H     -1.148313620414    0.110540120797    0.260354265449
               9 Cl    -0.667351330355   -2.702369861459    0.325752454622
              10 H      3.733493344186   -0.671414480444    0.683144397474
              11 H      2.174353769895   -2.593833861679    0.566757869655
              12 N      3.807010462784    2.809522516306    0.648639189585
              13 O      4.573098080716    3.735997061775    0.696381911192
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.080358249090   -0.049878174481    0.352708439898
               1 C      0.790387793527    1.025932087209    0.417626675513
               2 C      0.430543474217   -1.343482669005    0.407439272251
               3 C      2.174802076686    0.813518267904    0.536947839216
               4 C      2.667146653008   -0.502036333663    0.590762498828
               5 C      1.797305504227   -1.578692411594    0.525961551241
               6 H      0.404659690456    2.039189703579    0.376227036520
               7 C      3.063566363899    1.912029899355    0.602417553233
               8 H     -1.148964240299    0.112049286155    0.260246367858
               9 Cl    -0.667699115290   -2.702138973068    0.325516080597
              10 H      3.734684691173   -0.672200208258    0.683302104212
              11 H      2.175718760867   -2.594486515503    0.566909752282
              12 N      3.805322903628    2.807653186971    0.648371879793
              13 O      4.573013692992    3.737152854398    0.696332948560
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.080686994412   -0.049451807491    0.352694082372
               1 C      0.790304796308    1.026473953433    0.417608154703
               2 C      0.431304926340   -1.342514118719    0.407506506164
               3 C      2.174302277480    0.812444571946    0.536875074531
               4 C      2.667881533687   -0.502528415661    0.590813964434
               5 C      1.797740875716   -1.579183705447    0.526016880532
               6 H      0.404507339273    2.039648997613    0.376210159173
               7 C      3.063372776885    1.911152744581    0.602277897277
               8 H     -1.149158121587    0.113018295321    0.260245670755
               9 Cl    -0.667985355778   -2.701820317253    0.325483189614
              10 H      3.735337683314   -0.672831279616    0.683345058922
              11 H      2.176296201671   -2.594875654357    0.567020825712
              12 N      3.804429400657    2.806811335606    0.648203023276
              13 O      4.572482660445    3.738265400045    0.696469512535
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     4 
    Coordinates:
               0 C     -0.080725551799   -0.049380122621    0.352698235316
               1 C      0.790448192197    1.026603228206    0.417575578054
               2 C      0.431537883252   -1.342181192742    0.407554286282
               3 C      2.174261823318    0.812070840659    0.536811886106
               4 C      2.668081521062   -0.502713029716    0.590789022675
               5 C      1.797744479934   -1.579389865598    0.526027060645
               6 H      0.404662271695    2.039748703135    0.376163454832
               7 C      3.063511943366    1.910859628518    0.602198706292
               8 H     -1.149129856849    0.113311846977    0.260273156588
               9 Cl    -0.668137612522   -2.701672085158    0.325549160765
              10 H      3.735493339605   -0.673105176816    0.683302304225
              11 H      2.176268260483   -2.595057099644    0.567052320738
              12 N      3.804274397530    2.806809267815    0.648152959448
              13 O      4.571838908728    3.738705056985    0.696621868033
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     5 
    Coordinates:
               0 C     -0.080725551799   -0.049380122621    0.352698235316
               1 C      0.790448192197    1.026603228206    0.417575578054
               2 C      0.431537883252   -1.342181192742    0.407554286282
               3 C      2.174261823318    0.812070840659    0.536811886106
               4 C      2.668081521062   -0.502713029716    0.590789022675
               5 C      1.797744479934   -1.579389865598    0.526027060645
               6 H      0.404662271695    2.039748703135    0.376163454832
               7 C      3.063511943366    1.910859628518    0.602198706292
               8 H     -1.149129856849    0.113311846977    0.260273156588
               9 Cl    -0.668137612522   -2.701672085158    0.325549160765
              10 H      3.735493339605   -0.673105176816    0.683302304225
              11 H      2.176268260483   -2.595057099644    0.567052320738
              12 N      3.804274397530    2.806809267815    0.648152959448
              13 O      4.571838908728    3.738705056985    0.696621868033
